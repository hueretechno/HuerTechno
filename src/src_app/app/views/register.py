from flask import Blueprint, render_template, request
from app.models import db, Cultivo, Component, Device
from app.models import DetaileDevice
from app.models import CultivoDevice
from app.models import Broker
import json

register = Blueprint('register', __name__)


@register.route('/broker_config', methods=['GET', 'POST'])
def broker_config():
    print("broker config")
    data = [broker._asdict() for broker in db.session.query(Broker)]
    if request.form:
        print(request.form)
        db.session.add(broker_db(request.form))
        db.session.commit()
        broker_config = {
                'topic':request.form['topic'],
                'broker':request.form['ip_broker'],
                'port':request.form['port']}
        from app.background import create_sensor_mqtt, create_action_mqtt
        task_action = create_sensor_mqtt.delay(broker_config)
        task_action = create_action_mqtt.delay(broker_config)
    return render_template('register/broker_config.html', data=data)




@register.route('/cultivo', methods=['GET', 'POST'])
def cultivo():
    print("registrar cultivo")
    data = [cultivo._asdict() for cultivo in db.session.query(Cultivo)]
    if request.form:
        print(request.form)
        db.session.add(cultivo_db(request.form))
        db.session.commit()
    return render_template('register/cultivo.html', data=data)




@register.route('/device', methods=['GET', 'POST'])
def device():
    print("registrar device")
    data = [device._asdict() for device in db.session.query(Device)]
    if request.form:
        print(request.form)
        db.session.add(device_db(request.form))
        db.session.commit()
    return render_template('register/device.html', data=data)



@register.route('/component', methods=['GET', 'POST'])
def component():
    print("registrar component")
    data = [cultivo._asdict() for cultivo in db.session.query(Component)]
    if request.form:
        print(request.form)
        db.session.add(component_db(request.form))
        db.session.commit()
    return render_template('register/component.html', data=data)



@register.route('/device_component', methods=['GET', 'POST'])
def device_component():
    print("registrar detaile_device")
    data = dict()
    data['device'] = [device._asdict() for device in db.session.query(Device)]
    data['component'] = [cultivo._asdict() for cultivo in db.session.query(Component)]
    data['detaile_device'] = [dd for dd in db.session.query(DetaileDevice, Device, Component).join(Component).join(Device)]
    print(data['detaile_device'])
    if request.form:
        print(request.form)
        db.session.add(detaile_device_db(request.form))
        db.session.commit()
    return render_template('register/device_component.html', data=data)



@register.route('/cultivo_device', methods=['GET', 'POST'])
def cultivo_device():
    print("Asignar device to component")
    data = dict()
    data['device'] = [device._asdict() for device in db.session.query(Device)]
    data['cultivo'] = [cultivo._asdict() for cultivo in db.session.query(Cultivo)]
    data['cultivo_device'] = [cd for cd in db.session.query(CultivoDevice, Cultivo, Device).join(Cultivo).join(Device)]
    print(data['cultivo_device'])
    if request.form:
        print(request.form)
        db.session.add(cultivo_device_db(request.form))
        db.session.commit()
    return render_template('register/cultivo_device.html', data=data)






## helpers

def broker_db(data):
    print(data)
    broker = Broker()
    broker.ip_broker = data['ip_broker']
    broker.topic = data['topic']
    broker.port = data['port']
    return broker



def cultivo_db(data):
    print(data)
    cultivo = Cultivo()
    cultivo.name = data['name']
    cultivo.lenght = data['lenght']
    cultivo.hight = data['hight']
    cultivo.type_cultivo = data['type_cultivo']
    return cultivo


def component_db(data):
    print(data)
    component = Component()
    component.model = data['model']
    component.marca = data['marca']
    component.variable = data['variable']
    component.function = data['function']
    component.num_pin = data['num_pin']
    return component



def device_db(data):
    print(data)
    device = Device()
    device.model = data['model']
    device.marca = data['marca']
    device.function = data['function']
    return device


def detaile_device_db(data):
    print(data)
    dd = DetaileDevice()
    dd.device_id = data['device']
    dd.component_id = data['component']
    return dd


def cultivo_device_db(data):
    print(data)
    cd = CultivoDevice()
    cd.cultivo_id = data['cultivo']
    cd.device_id = data['device']
    cd.state = ''
    return cd



