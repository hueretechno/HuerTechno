from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy import  Column, Integer, JSON, DateTime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from collections import OrderedDict


db = SQLAlchemy()


class Base(db.Model):

    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())


    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result




class Broker(db.Model):

    __tablename__ = 'broker'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    port = db.Column(db.Unicode(128))
    ip_broker = db.Column(db.Unicode(128))
    topic = db.Column(db.Unicode(128))


    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result





class Cultivo(db.Model):

    __tablename__ = 'cultivo'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    name = db.Column(db.Unicode(128))
    hight = db.Column(db.Integer, nullable=False)
    lenght = db.Column(db.Integer, nullable=False)
    type_cultivo = db.Column(db.Unicode(128)) # hydro

    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result





class ComponentCultivo(Base):

    __tablename__ = 'component_cultivo'
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = relationship('Component')
    state = db.Column(db.Unicode(2), nullable=False) #1/0





class CultivoDevice(Base):

    __tablename__ = 'cultivo_device'
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    device = relationship('Device')
    state = db.Column(db.Unicode(2), nullable=False) #1/0





class DetaileDevice(Base):

    __tablename__ = 'detaile_device'
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    device = relationship('Device')
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = relationship('Component')




class Device(db.Model):

    __tablename__ = 'device'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    model = db.Column(db.Unicode(128))
    marca = db.Column(db.Unicode(128))
    function = db.Column(db.Unicode(128))

    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result





class Action(db.Model):

    __tablename__ = 'action'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    time_action  = db.Column(db.DateTime)
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = relationship('Component')
    state = db.Column(db.Unicode(2), nullable=False) #1/0
    accion = db.Column(db.Unicode(3), nullable=False) #1/0

    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result




class Measure(db.Model):

    __tablename__= 'measure'
    id = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = relationship('Component')
    variable = db.Column(db.Unicode(128))
    value = db.Column(db.Unicode(10), nullable=False)




class Component(Base):

    __tablename__ = 'component'
    model = db.Column(db.Unicode(128))
    marca = db.Column(db.Unicode(128))
    precision = db.Column(db.Unicode(128))
    variable = db.Column(db.Unicode(128))
    function = db.Column(db.Unicode(128))
    state = db.Column(db.Unicode(2))
    num_pin = db.Column(db.Unicode(2))




class Huerta(Base):

    __tablename__ = 'huerta'
    hight = db.Column(db.Integer, primary_key=True)
    lenght = db.Column(db.Integer, primary_key=True)
    type_huerta = db.Column(db.Unicode(128)) # indoor/outdoor




class User(Base):

    __tablename__ = 'user'
    mail = db.Column(db.Unicode(128))
    password = db.Column(db.Unicode(128))




class Recipe(Base):

    __tablename__ = 'recipe'
    info_recipe = db.Column(JSON)




class DetaileCultivo(Base):

    __tablename__ = 'detaile_cultivo'
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    plant_id = db.Column(db.Integer, db.ForeignKey('plant.id'))
    plant = relationship('Plant')
    cantidad = db.Column(db.Unicode(128))




class Book(Base):

    __tablename__ = 'book'
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'))
    recipe = relationship('Recipe')



class CultivoCiclo(Base):

    __tablename__ = 'planta_ciclo'
    ciclo_vegetativo_id = db.Column(db.Integer, db.ForeignKey('ciclo_vegetativo.id'))
    ciclo_vegetativo = relationship('CicloVegetativo')
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')




class Plant(Base):

    __tablename__ = 'plant'
    varieti_id = db.Column(db.Integer, db.ForeignKey('varieti.id'))
    varieti = relationship('Varieti')



class Specie(Base):

    __tablename__ = 'specie'
    especie_id =  db.Column(db.Unicode(128))
    especie =  db.Column(db.Unicode(128))



class Varieti(Base):

    __tablename__ = 'varieti'
    genotipo = db.Column(db.Unicode(128))
    cruce = db.Column(db.Unicode(128))
    production = db.Column(db.Unicode(128))
    specie_id = db.Column(db.Integer, db.ForeignKey('specie.id'))
    specie = relationship('Specie')




class CicloVegetativo(Base):

    __tablename__ = 'ciclo_vegetativo'
    fase = db.Column(db.Unicode(128))
    inicio = db.Column(db.Unicode(20))
    fin = db.Column(db.Unicode(20))




class Umbral(Base):

    __tablename__ = 'umbral'
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    variable = db.Column(db.Unicode(128))
    limit_inf = db.Column(db.Unicode(128))
    limit_max = db.Column(db.Unicode(128))
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = relationship('Component')



class UserCultivo(Base):

    __tablename__ = 'user_cultivo'
    role = db.Column(db.Unicode(128))
    last_conexion  = db.Column(db.DateTime)
    cultivo_id = db.Column(db.Integer, db.ForeignKey('cultivo.id'))
    cultivo = relationship('Cultivo')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = relationship('User')


