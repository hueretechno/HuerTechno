from paho.mqtt.client import *



class ClientMqtt(Client):


    def __init__(self):
        Client.__init__(self, client_id="", clean_session=True, userdata=None,
                protocol=MQTTv311, transport="tcp")
        self._data_payload = dict()
        self._data_topic = ""
        self._state_msg = None



    def process_msg(self, msg):
        # topic: Cultivo_1/Sensors/DHT22/TEMP
        # topic: Cultivo_2/Action/RIEGO/Bomba1
        topic = msg.topic
        if topic:
            cultivo = topic.split('/')[0]
            function = topic.split('/')[1]
            component = topic.split('/')[2]
            variable = topic.split('/')[3]
            value = msg.payload.decode("utf-8")
            self._data_payload = {
                'cultivo':cultivo,
                'function':function,
                'component':component,
                'variable':variable,
                'value':value
                       }
        return True



