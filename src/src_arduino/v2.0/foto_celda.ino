/*
 * Definiciones del componente foto celda
 */
#define FOTOPIN D3

void foto_celda_setup(){
  pinMode(FOTOPIN, INPUT);
}

void foto_celda_loop(){
  char FOTO_CELDA_measure[6]= "";
  float foto_celda = foto_celda_sensor();
  sprintf(FOTO_CELDA_measure, "%.02f", foto_celda);
  pub("1/SENSOR/4/LUZ", FOTO_CELDA_measure);
}

float foto_celda_sensor(){
  float measure;
  measure = digitalRead(FOTOPIN);
  if (isnan(measure)){
    Serial.println();
    Serial.print("Error YL69:");
    Serial.println();    
  }
  return measure;
}
